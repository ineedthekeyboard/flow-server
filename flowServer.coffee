###Needed Assets###
express = require('express')
mongoose = require('mongoose')
bodyParser = require('body-parser')
js2xmlparser = require('js2xmlparser')
Bar = require('./models/bar')

app = express()
mongoose.connect('mongodb://127.0.0.1:27017/bars');
app.use(bodyParser.json())
port = process.env.PORT || 8080

###define router and routes###
router = express.Router()
barsRoute = router.route('/bars')
barsXmlRoute = router.route('/bars/xml')
barRoute = router.route('/bars/:bar_id')

###dummy route###
router.get('/', (req,res) ->
  res.json({message: 'Server is working properly'}))

###Bar Routes###

###Add a new bar###
barsRoute.post((req,res)->
  bar = Bar()
  bar.name = req.body.address
  bar.rating = req.body.rating
  bar.address = req.body.address
  bar.latitude = req.body.latitude
  bar.longitude = req.body.longitude
  bar.typeification = req.body.typeification

  bar.save((er)->
    res.send(er) if er
    res.json({message: 'A New Bar was added', data:bar})))

###Get All Bars###
barsXmlRoute.get((req,res)->
  Bar.find((err,bars)->
    res.send(err) if (err)
    ###res.json(bars)###
    res.set('Content-Type', 'text/xml')
    res.send(js2xmlparser("Bar", JSON.stringify(bars)))))

barsRoute.get((req,res)->
  Bar.find((err,bars)->
    res.send(err) if (err)
    ###res.json(bars)###
    res.json(bars)))

###Get a specific bar###
barRoute.get((req,res)->
  Bar.findById(req.params.bar_id,(err,bar)->
    res.send(err) if err
    res.json(bar)))

app.use('/api',router)
app.listen(port)
console.log('Flow Server Started on port: ' + port)